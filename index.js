const http=require('http');


const port = 4000

const userData = [

       {
           "firstName": "Mary Jane",
           "latName": "Dela Cruz",
           "mobileNo": "09123456789",
           "email": "mjdelacruz@mail.com",
           "password": 123
       },
       {
            "firstName": "John",
            "lastName": "Doe",
            "mobileNo": "09123456789",
            "email": "jdoe@mail.com",
            "password": 123
       }
       
]

const server = http.createServer((request,response) => {	


		if (request.url == '/profile' && request.method == "GET"){
			response.writeHead(200, {'Content-Type': 'application/JSON'})
			response.write(JSON.stringify(userData));
			response.end()
		}

		if (request.url == '/register' && request.method == "POST"){
			let requestBody = "";
			request.on('data', function(data){
				requestBody += data
			})

			request.on('end', function(){
				console.log(typeof requestBody);
				requestBody = JSON.parse(requestBody);

				let newUser = {
					"name": requestBody.name,
					"email": requestBody.email
				}

				userData.push(newUser)
				console.log(userData)

				response.writeHead(200, {'Content-Type': 'application/JSON'})
				response.write(JSON.stringify(newUser));
				response.end()
			})	
		}

});

server.listen(port);


console.log(`Server now accessible at localhost: ${port}`)	
